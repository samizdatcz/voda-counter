function zeroPad(num) {
  num = num.toString()
  if (num.length < 2) {
    return '0' + num;
  } else {
    return num;
  };
};

function niceDay(days) {
  if (days == 1) {
    return ' den';
  } else if (days > 1 & days < 5) {
    return ' dny';
  } else {
    return ' dnů';
  };
};

function niceNum(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

// (m3 za 24 h / 86400 (sec)) * 1000 = l/sec
var spotrebaWc = (214570.35 / 86400) * 1000;
var spotrebaPotrubi = (246090 / 86400) * 1000;
var spotrebaZalivka = (46645.73 / 86400) * 1000;
var spotrebaBazeny = (13300000 / 86400) * 1000;

var spotrebaPiti = 26413.21 * 1000; //litru za den

var startStamp = 1498024800; //start 21. 6. 2017 08:00

function makeCount() {
  var elapsed = Math.floor(Date.now() / 1000) - startStamp; //elapsed time in sec

  if (elapsed < 0) { //wait till start
    console.log(zeroPad(Math.floor(elapsed / 3600)) + ':' + zeroPad(Math.floor((elapsed % 3600) / 60)) + ':' + zeroPad(elapsed % 60))
    return;
  };

  if (elapsed > 86400) { //capped to 24 h
    elapsed = 86400;
  };

  d3.select('#splachovani').text(niceNum(Math.round(spotrebaWc * elapsed)))
  d3.select('#potrubi').text(niceNum(Math.round(spotrebaPotrubi * elapsed)))
  d3.select('#zalevani').text(niceNum(Math.round(spotrebaZalivka * elapsed)))
  d3.select('#bazeny').text(niceNum(Math.round(spotrebaBazeny * elapsed)))

  d3.select('#cas').text(zeroPad(Math.floor(elapsed / 3600)) + ':' + zeroPad(Math.floor((elapsed % 3600) / 60)) + ':' + zeroPad(elapsed % 60))

  var sum = (spotrebaWc * elapsed) + (spotrebaPotrubi * elapsed) + (spotrebaZalivka * elapsed) + (spotrebaBazeny * elapsed)
  
  d3.select('#piti').text(niceNum(Math.round(sum / spotrebaPiti)) + niceDay(Math.round(sum / spotrebaPiti)))
  
  if (elapsed >= 86400) { //24 h
    clearInterval(runner);
  };
};
makeCount();
var runner = setInterval(makeCount, 1000);